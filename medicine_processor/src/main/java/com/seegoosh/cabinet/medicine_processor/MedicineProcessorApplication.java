package com.seegoosh.cabinet.medicine_processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicineProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicineProcessorApplication.class, args);
	}

}
