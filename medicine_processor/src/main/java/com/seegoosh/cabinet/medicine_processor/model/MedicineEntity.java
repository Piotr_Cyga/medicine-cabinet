package com.seegoosh.cabinet.medicine_processor.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class MedicineEntity {

    @Id
    @Column(name = "GTIN")
    Long gtin;

    @Column(name = "trade_name")
    String name;

    @Column(name = "manufacturer")
    String manufacturer;

    @Column(name = "form")
    String form;

    @Column(name = "dose")
    String dose;

    @Column(name = "packaging")
    String packaging;

    @Column(name = "BAZYL")
    Integer bazyl;
}
